import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppService } from '../app.service';
import { firstValueFrom } from 'rxjs';
import { RouterModule } from '@angular/router';
import { ShellComponent } from '../shell/shell.component';

@Component({
  selector: 'org-payment',
  standalone: true,
  imports: [CommonModule, RouterModule, ShellComponent],
  templateUrl: './payment.component.html',
  styleUrl: './payment.component.scss',
})
export class PaymentComponent {
  isProcessing = false;
  success = false;
  title = 'Pay';
  sleep_seconds = 2;
  btnStatus = `{
    'btn': true,
    'btn-normal': !isProcessing && !success,
    'btn-processing': isProcessing,
    'btn-success': success,
  }`;

  constructor(private appService: AppService) {}

  async paymentMethod() {
    if (this.success) return;

    this.isProcessing = true;
    this.title = 'Procesing...';

    try {
      await firstValueFrom(
        this.appService.payment({ sleep_seconds: this.sleep_seconds })
      );
      this.success = true;
      this.title = 'Success!';
    } catch (error: any) {
      this.title = 'Pay';
      console.error('error', error);
      alert(error.error.error_msg);
    } finally {
      this.isProcessing = false;
    }
  }
}
