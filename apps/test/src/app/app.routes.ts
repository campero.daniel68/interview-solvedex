import { Route } from '@angular/router';

import { Shell } from './shell/shell.service';
import { NxWelcomeComponent } from './nx-welcome.component';
import { PaymentComponent } from './payment/payment.component';

export const appRoutes: Route[] = [
  Shell.childRoutes([
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: NxWelcomeComponent },
  ]),
  Shell.childRoutes([
    { path: 'payment', component: PaymentComponent },
  ]),
];
