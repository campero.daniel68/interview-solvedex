import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { Router, RouterModule } from '@angular/router';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { AppService } from '../app.service';
import { HttpClientModule } from '@angular/common/http';

@Component({
  selector: 'org-shell',
  standalone: true,
  imports: [CommonModule, MaterialModule, HttpClientModule, RouterModule],
  templateUrl: './shell.component.html',
  styleUrl: './shell.component.scss',
})
export class ShellComponent implements OnInit {
  menuItems = [
    {
      name: 'Home',
      link: 'home',
      icon: 'home',
      children: [],
    },
    {
      name: 'Payment',
      link: 'payment',
      icon: 'payment',
      children: [],
    }
  ];
  version: string | null = '0.0.1';

  constructor(
    private router: Router,
    private breakpoint: BreakpointObserver,
    private appService: AppService
  ) {}

  ngOnInit(): void {
      this.appService.testApi().subscribe((data) => {
        console.log(data)
      });
  }

  logout() {
    this.router.navigate(['/'], { replaceUrl: true })
  }

  get username(): string | null {
    return 'test';
  }

  get isMobile(): boolean {
    return this.breakpoint.isMatched(Breakpoints.Small) || this.breakpoint.isMatched(Breakpoints.XSmall);
  }

  get title(): string {
    return 'My app';
  }
}
