import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const routes = {
  test: () => `/api/test`,
  payment: () => `/api/testApp/payment`,
};

@Injectable({
  providedIn: 'root',
})
export class AppService {
  constructor(private httpClient: HttpClient) {}

  /**
   * Get all Miembros
   * @return A list of Miembros
   */
  testApi() {
    return this.httpClient.get(routes.test());
  }

  /**
   * Post Payment
   * @return A data payment
   */
  payment(paymentData: { "sleep_seconds": number }) {
    return this.httpClient.post(routes.payment(), paymentData);
  }
  
}
