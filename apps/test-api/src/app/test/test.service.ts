import { Injectable } from '@nestjs/common';
import { CreateTestDto } from './dto/create-test.dto';
import { UpdateTestDto } from './dto/update-test.dto';

@Injectable()
export class TestService {
  create(createTestDto: CreateTestDto) {
    return 'This action adds a new test';
  }

  findAll() {
    return `This action returns all test`;
  }

  findOne(id: number) {
    return `This action returns a #${id} test`;
  }

  update(id: number, updateTestDto: UpdateTestDto) {
    return `This action updates a #${id} test`;
  }

  remove(id: number) {
    return `This action removes a #${id} test`;
  }

  payment(paymentData: { sleep_seconds: number }) {
    console.log(paymentData.sleep_seconds);
    return new Promise((resolve, reject) => {
      const myInterval = setInterval(() => {
        clearInterval(myInterval);
        if (paymentData.sleep_seconds > 10) {
          reject({
            message: `Sleep seconds(${paymentData.sleep_seconds}) greater than 10.`,
          });
        }
        resolve(paymentData);
      }, paymentData.sleep_seconds * 1000);
    });
  }
}
